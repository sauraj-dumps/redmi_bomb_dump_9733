#
# Copyright (C) 2023 The LineageOS Project
#
# SPDX-License-Identifier: Apache-2.0
#

PRODUCT_MAKEFILES := \
    $(LOCAL_DIR)/lineage_bomb.mk

COMMON_LUNCH_CHOICES := \
    lineage_bomb-user \
    lineage_bomb-userdebug \
    lineage_bomb-eng
