#
# Copyright (C) 2023 The LineageOS Project
#
# SPDX-License-Identifier: Apache-2.0
#

# Enable updating of APEXes
$(call inherit-product, $(SRC_TARGET_DIR)/product/updatable_apex.mk)

# Include GSI keys
$(call inherit-product, $(SRC_TARGET_DIR)/product/gsi_keys.mk)

# fastbootd
PRODUCT_PACKAGES += \
    android.hardware.fastboot@1.1-impl-mock \
    fastbootd

# Health
PRODUCT_PACKAGES += \
    android.hardware.health@2.1-impl \
    android.hardware.health@2.1-impl.recovery \
    android.hardware.health@2.1-service

# Overlays
PRODUCT_ENFORCE_RRO_TARGETS := *

# Partitions
PRODUCT_BUILD_SUPER_PARTITION := false
PRODUCT_USE_DYNAMIC_PARTITIONS := true

# Product characteristics
PRODUCT_CHARACTERISTICS := default

# Rootdir
PRODUCT_PACKAGES += \
    setup_rcv2mainmic.sh \
    cpu_temp_check.sh \
    capture.sh \
    mishow.sh \
    install-recovery.sh \
    playback_headset.sh \
    setup_topmic2headphone.sh \
    setup_rcv2topmic.sh \
    setup_headsetmic2headphone.sh \
    capture_headsetmic.sh \
    setup_headsetmic2rcv.sh \
    playback.sh \
    teardown_loopback.sh \
    setup_mainmic2headphone.sh \
    init.insmod.sh \

PRODUCT_PACKAGES += \
    fstab.emmc \
    init.connectivity.rc \
    meta_init.modem.rc \
    init.sensor_2_0.rc \
    init.connectivity.common.rc \
    multi_init.rc \
    init.mt6873.rc \
    factory_init.connectivity.rc \
    meta_init.project.rc \
    init.project.rc \
    init.modem.rc \
    factory_init.rc \
    init_connectivity.rc \
    factory_init.project.rc \
    init.cgroup.rc \
    meta_init.connectivity.rc \
    meta_init.connectivity.common.rc \
    meta_init.rc \
    init.aee.rc \
    init.mt6873.usb.rc \
    init.ago.rc \
    factory_init.connectivity.common.rc \

PRODUCT_COPY_FILES += \
    $(LOCAL_PATH)/rootdir/etc/fstab.emmc:$(TARGET_COPY_OUT_RAMDISK)/fstab.emmc

# Shipping API level
PRODUCT_SHIPPING_API_LEVEL := 29

# Soong namespaces
PRODUCT_SOONG_NAMESPACES += \
    $(LOCAL_PATH)

# Inherit the proprietary files
$(call inherit-product, vendor/xiaomi/bomb/bomb-vendor.mk)
